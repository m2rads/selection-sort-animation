const canvas = document.getElementById("canvas");
const ctx = canvas.getContext('2d');

const listWidth = 40;
const listHeight = 40;

let Arr = [43, 35, 12, 54, 16];

let xPos = canvas.width/2 - 5 * (listWidth/2);
let yPos = canvas.height/2 - (listHeight/2);

let xTxtPos = xPos + 11;

//ctx.StrokeRect(xPos, yPos, listWdth, listHeight);

console.log(xPos);
console.log(yPos);

function ListElement(color, txt) {
    this.x = xPos;
    this.y = yPos;
    this.w = listWidth;
    this.h = listHeight;
    this.c = color;
    this.tPos = xTxtPos;
    this.txt = txt;

    this.draw = function () {
        ctx.StrokeStyle = this.c;
        ctx.strokeRect(this.x, this.y, this.w, this.h);
        ctx.font = "25px Arial";
        ctx.fillStyle = 'black';
        ctx.fillText(`${this.txt}`, this.tPos, 85.5, 20);
    }
}

// create the element objects
const List = []
for (let i = 0; i < Arr.length; i++) {
    let c = "black"
    List.push(new ListElement(c, Arr[i]));
    xPos += listWidth;
    xTxtPos += listWidth;
}

// draw the list with it's items
for (let i = 0; i < List.length; i++){
    List[i].draw();
}

// selection sort
for (let i = 0; i < Arr.length; i++) {
    // model array
    let currentMinValue = Arr[i];
    let currentMinIndx = i; 

    // presentation array
    let currentMinElem = List[i];
    let currentMinElemIndix = i;

    for (let j = i +1; j < Arr.length; j++) {
        if (currentMinValue > Arr[j]) {
            // model array
            currentMinValue = Arr[j];
            currentMinIndx = j;

            // presentation array
            currentMinElem = List[j];
            currentMinElemIndix = j;
        }
    }
    
    if (currentMinIndx != i) {
        // model array
        Arr[currentMinIndx] = Arr[i];
        Arr[i] = currentMinValue;

        // presentation array
        List[currentMinElemIndix] = List[i];
        List[i] = currentMinElem;

        swapAnimate(currentMinElem, List[j]);
    }    
}

function swapAnimate (currentMinElem, nextMinElem) {
    let x0 = currentMinElem;
    let x1 = nextMinElem;

    while (currentMinVal >= x1 && nextMinElem <= x0) {
        currentMinElem.this.x += 5;
        nextMinElem.this.x -=5; 
    }
}
